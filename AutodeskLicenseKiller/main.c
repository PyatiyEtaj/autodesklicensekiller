#include <stdio.h>
#include <string.h>
#include <Windows.h>

#define ARE_STRINGS_EQUAL(x, y) strcmp(x, y) == 0

typedef struct {
	const char* text;
	const char* class;
	int mode;
} rule, * prule;

BOOL WINAPI enum_windows_proc(HWND hwnd, LPARAM lp)
{
	prule rules = (prule)lp;
	char text [MAX_PATH];
	char cls  [MAX_PATH];
	GetWindowTextA(hwnd, (LPSTR)&text, MAX_PATH);
	GetClassNameA (hwnd, (LPSTR)&cls , MAX_PATH);

	if (
		(strnlen_s(rules->text , MAX_PATH) < 1 || ARE_STRINGS_EQUAL(rules->text , text)) &&
		(strnlen_s(rules->class, MAX_PATH) < 1 || ARE_STRINGS_EQUAL(rules->class, cls ))
	) {
		ShowWindow(hwnd, rules->mode);
#ifdef _DEBUG
		printf_s("[+]	");
#endif
	}
#ifdef _DEBUG
	printf_s("text[%s] - class[%s]\n", text, cls);
#endif

	return TRUE;
}

int main(int argc, char* argv[]) {
#ifdef _DEBUG
	system("chcp 1251");
#endif
	int mode = argc >= 2 && ARE_STRINGS_EQUAL(argv[1], "show") ? SW_SHOW : SW_HIDE;
	char* rule_text  = argc >= 3 ? argv[2] : "Autodesk Notification";
	char* rule_class = argc >= 4 ? argv[3] : "CefBrowserWindow";
	rule result = { rule_text, rule_class, mode };
	EnumWindows(enum_windows_proc, (LPARAM)&result);

	return 0;
}